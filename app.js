const mail = document.querySelector('#mail');
const password = document.querySelector('#pass');
const buttonSignup = document.querySelector('#button-signup');

buttonSignup.addEventListener('click', ()=>{
  var correo = mail.value;
  var contrasena = password.value;
  firebase.auth().createUserWithEmailAndPassword(correo, contrasena).then(() => {
  alert("Usuario ingresado con exito");
  mail.value = '';
  password.value = '';
  }).catch((error) => {
    var errorCode = error.code;
    var errorMessage = error.message;
    alert("Error al registrar usuario: "+ errorMessage);
  });
});